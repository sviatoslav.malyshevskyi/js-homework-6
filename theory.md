Вопрос: 
Опишите своими словами как работает цикл forEach.

Ответ:
Цикл forEach - это метод который выполняет заданную функцией задачу для каждого элемента в массиве. Этот цикл проходит 
по каждому элементу в том порядке, в котором они указаны в массиве один раз, выполняет заданную функцию, ничего не возвращает 
и заканчивается после последнего элемента массива. 
