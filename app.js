'use strict';

let arr = ['hello', 'world', 23, '23', null,];
let dataType = prompt("Type of data:", 'string');
let newArray = filterBy(arr, dataType);
console.log(newArray);

function filterBy(arr, dataType) {
    return arr.filter(function (input) {
        if (dataType === 'string') {
            return typeof input !== 'string';
        } else if (dataType === 'number') {
            return typeof input !== 'number';
        } else if (dataType === 'symbol') {
            return typeof input !== 'symbol';
        } else if (dataType === 'boolean') {
            return typeof input !== 'boolean';
        } else if (dataType === 'null') {
            return typeof input !== 'null';
        } else if (dataType === 'undefined') {
            return typeof input !== 'undefined';
        } else if (dataType === 'object') {
            return !(typeof input === 'object' && input !== null);
        }
    });
}